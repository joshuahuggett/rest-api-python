import json, datetime, redis
from hotqueue import HotQueue
from jobs import readjson, stats, stats_range, stats_mean, stats_add


q=HotQueue('queue', host='redis', port=6379, db=1)
r=redis.StrictRedis(host='redis', port=6379, db=2)

@q.worker
def work(task):
	task=json.loads(task)
	task['timestart']=datetime.datetime.now().strftime('%Y%m%d%H%M%S')
	task['status']='task in progress'
	r.set(task['uid'], json.dumps(task))
	if task['commandtype']=='01':
		task['return']=json.loads(stats(task['commandparameter']))
	if task['commandtype']=='02':
		task['return']=json.loads(stats_range(*task['commandparameter']))
	if task['commandtype']=='03':
		task['return']=json.loads(stats_mean(*task['commandparameter']))
	if task['commandtype']=='04':
		task['return']=json.loads(stats_add(task['commandparameter']))
	task['timeend']=datetime.datetime.now().strftime('%Y%m%d%H%M%S')
	task['status']='task finished'
	r.set(task['uid'], json.dumps(task))

work()
