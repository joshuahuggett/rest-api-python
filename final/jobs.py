import json

def readjson(filename):
	with open(filename, 'r') as infile:
		data=json.load(infile)
	return data

def stats(parameter0):
	data=readjson('statistics.txt')
	a=list()
	for x in data['statistics']:
		if x['playerid']==parameter0:
			a.append(x)
	return json.dumps(a)

def stats_range(parameter0,parameter1,parameter2):
	data=readjson('statistics.txt')
	a=list()
	for x in data['statistics']:
		if x['playerid']==parameter0:
			if int(parameter2)>=int(x['seasonid'])>=int(parameter1):
				a.append(x)
	return json.dumps(a)

def stats_mean(parameter0,parameter1):
	data=readjson('statistics.txt')
	a=list()
	for x in data['statistics']:
		if x['playerid']==parameter0:	
			a.append(float(x[parameter1]))
	return str(sum(a)/len(a))

def stats_add(parameter0):
	data=readjson('statistics.txt')
	data['statistics'].append(parameter0)
	with open ('statistics.txt', 'w') as outfile:
		json.dump(data,outfile)	
