import json, uuid, datetime, redis
from hotqueue import HotQueue
from flask import Flask, request

app = Flask(__name__)
q=HotQueue('queue', host='redis', port=6379, db=1)
r=redis.StrictRedis(host='redis', port=6379, db=2)

@app.route('/', methods=['GET'])
def hello_world():
	return 'Hello World\n'

@app.route('/stats/', methods=['GET'])
def stats():
	commandtype='01'
	commandparameter=request.args.get('parameter0')
	status='task in queue'
	uid=uuid.uuid4().hex
	time=datetime.datetime.now().strftime('%Y%m%d%H%M%S')
	task={'commandtype':commandtype, 'commandparameter':commandparameter,'status':status, 'uid':uid, 'time':time, 'timestart':None, 'timeend':None, 'return':None}
	q.put(json.dumps(task))
	r.set(uid, json.dumps(task))
	return'task uid {}\n'.format(uid)

@app.route('/stats/range/', methods=['GET'])
def stats_range():
	commandtype='02'
	commandparameter=[request.args.get('parameter0'), request.args.get('parameter1'), request.args.get('parameter2')]
	status='task in queue'
	uid=uuid.uuid4().hex
	time=datetime.datetime.now().strftime('%Y%m%d%H%M%S')
	task={'commandtype':commandtype, 'commandparameter':commandparameter,'status':status, 'uid':uid, 'time':time, 'timestart':None, 'timeend':None, 'return':None}
	q.put(json.dumps(task))
	r.set(uid, json.dumps(task))
	return'task uid {}\n'.format(uid)

@app.route('/stats/mean/', methods=['GET'])
def stats_mean():
	commandtype='03'
	commandparameter=[request.args.get('parameter0'), request.args.get('parameter1')]
	status='task in queue'
	uid=uuid.uuid4().hex
	time=datetime.datetime.now().strftime('%Y%m%d%H%M%S')
	task={'commandtype':commandtype, 'commandparameter':commandparameter,'status':status, 'uid':uid, 'time':time, 'timestart':None, 'timeend':None, 'return':None}
	q.put(json.dumps(task))
	r.set(uid, json.dumps(task))
	return'task uid {}\n'.format(uid)

@app.route('/stats/add/', methods=['POST'])
def stats_add():
	commandtype='04'
	commandparameter=request.json
	status='task in queue'
	uid=uuid.uuid4().hex
	time=datetime.datetime.now().strftime('%Y%m%d%H%M%S')
	task={'commandtype':commandtype, 'commandparameter':commandparameter,'status':status, 'uid':uid, 'time':time, 'timestart':None, 'timeend':None, 'return':None}
	q.put(json.dumps(task))
	r.set(uid, json.dumps(task))
	return '', 204

@app.route('/uid/', methods=['GET'])
def uid():
	uid=request.args.get('uid')
	task=json.loads(r.get(uid))
	return json.dumps(task)

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')

