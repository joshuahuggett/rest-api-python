# Project Overview
This API was developed to query the regular season statistics for Michael Jordan and Scottie  Pippen. In 1995, the Chicago Bulls set the all-time NBA record with 72 wins, the first team ever to break the 70-win mark. That year, Michael Jordan led the league in scoring and was the regular season MVP, Finals MVP, and made the all-NBA first team and all-defensive first team. Scottie Pippen also made the all-NBA first team and all-defensive first team. This API is meant to demonstrate the impact of Michael Jordan and Scottie Pippen on that Chicago Bulls team. 

Because the data is provided by the National Basketball Association, and is far more extensive, it is unlikely  that this API will be monetized; however, if the express written consent of the NBA was given and the database were more extensive, clients could be charged for access to the API and data based on the size of data that they are requesting. Requests that query all regular season statistics transmit significantly larger data than requests that query subsets of regular season statistics or the mean of the regular season statistics. Similarly, requests that access current regular season statistics could be monetized since there is a greater demand and requests that access past regular seasons would not be monetized since there is a lesser demand. However, I do not believe a resource like this should be monetized, and the data should remain accessible. 

# How to Build

After cloning the repository:

`cd final`

`docker build -t statistics:latest .`

# How to Run

`docker-compose up -d`

# API Specification

## [GET] /stats/ 

### Description
 Get all regular season statistics for Michael Jordan or Scottie Pippen  
 
### Parameters
 893 - playerid for Michael Jordan  
 937 - playerid for Scottie Pippen

### Example
 input `curl -X GET localhost:5011/stats/?parameter0=893`  
 output `task uid f99848cbd651485e9befd648c9a5b750`

## [GET] /stats/range/

### Description
 Get subset of regular season statistics for Michael Jordan or Scottie Pippen, given start and end years  
 
### Parameters
 [1984, 2002] - range of regular season statistics for Michael Jordan  
 [1987, 2003] - range of regular season statistics for Scottie Pippen
 
### Example
 input `curl -X GET "localhost:5011/stats/range/?parameter0=893&parameter1=1991&parameter2=1993"`  
 output `task uid 6546c98adfe3427eb8ddcc559bfb6f1c`

## [GET] /stats/mean/

### Description
 Get the mean of the regular season statistics for Michael Jordan or Scottie Pippen  
  
### Parameters
 gamesplayed, gamesstarted, minutesplayed, fieldgoalsmade, fieldgoalsattempted, fieldgoalpercentage, threepointfieldgoalsmade, threepointfieldgoalsattempted, threepointfieldgoalpercentage, freethrowsmade, freethrowsattempted, freethrowpercentage, offensiverebounds, defensiverebounds, rebounds, assists, steals, blocks, turnovers, personalfouls, and points.

### Example
 input `curl -X GET "localhost:5011/stats/mean/?parameter0=893&parameter1=points"`  
 output `task uid df5644081eac4c3dbdc85dee976959fd`

## [POST] /stats/add/

### Description
 Post the regular season statistics for Michael Jordan or Scottie Pippen  

### Example
 input `curl -i -H "Content-Type: application/json" -X POST -d '{"playerid":"893","seasonid":"2020","leagueid":"00","teamid":"1610612764","teamabbreviation":"WAS","playerage":"40.0","gamesplayed":"82","gamesstarted":"67","minutesplayed":"36.90000000000000000","fieldgoalsmade":"8.300000","fieldgoalsattempted":"18.600000","fieldgoalpercentage":"0.445","threepointfieldgoalsmade":"0.200000","threepointfieldgoalsattempted":"0.700000","threepointfieldgoalpercentage":"0.291","freethrowsmade":"3.200000","freethrowsattempted":"4.000000","freethrowpercentage":"0.821","offensiverebounds":"0.900000","defensiverebounds":"5.200000","rebounds":"6.100000","assists":"3.800000","steals":"1.500000","blocks":"0.500000","turnovers":"2.100000","personalfouls":"2.100000","points":"20.000000"}' localhost:5011/stats/add/`  
 output `HTTP/1.0 204 NO CONTENT  
         Content-Type: text/html; charset=utf-8  
         Server: Werkzeug/1.0.1 Python/2.7.17  
         Date: Wed, 13 May 2020 19:36:41 GMT`  

## [GET] /uid/

### Description
 Get the return of a task
 
   
### Parameters
  task uid
 
### Example
  input `curl -X GET localhost:5011/uid/?uid=df5644081eac4c3dbdc85dee976959fd`  
  output `{"status": "task finished", "commandtype": "03", "commandparameter": ["893", "points"], "uid": "df5644081eac4c3dbdc85dee976959fd", "timeend": "20200513193342", "timestart": "20200513193342", "time": "20200513193342", "return": 29.4533333333}`
  

# How to Stop
 ` docker-compose down`
